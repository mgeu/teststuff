package de.mineguild.nylser.main;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class TestStuffGui extends JPanel {
	
	public TestStuffGui(ArrayList<String> mods, ArrayList<String> modsLinks){
		setLayout(new GridLayout(mods.size() + 1,1));
		JLabel l = new JLabel("These spare mods still need to be downloaded: (Click the button to download)");
		add(BorderLayout.NORTH, l);
		for(int i = 0; i < modsLinks.size(); i++){
			final String link = modsLinks.get(i);
			JButton b = new JButton(mods.get(i));
			b.setSize(10,10);
			
			b.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						Desktop.getDesktop().browse(new URI(link));
					} catch (IOException | URISyntaxException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			add(b);
		}

	}
	
	public static void main(final ArrayList<String> spareMods, final ArrayList<String> spareModsLinks){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(new Dimension(300,300));
				frame.add(new TestStuffGui(spareMods, spareModsLinks));
				frame.setVisible(true);
			}
		});
	}

}
