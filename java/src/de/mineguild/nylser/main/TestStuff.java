package de.mineguild.nylser.main;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.yaml.snakeyaml.Yaml;

public class TestStuff {
	public static void main(String[] args) throws Exception{
		
		
		int value = JOptionPane.showConfirmDialog(null, "Your modpack seems to be out of date... Do you want to update it?");
		System.out.println(value);
		
		if(!(value < 1)){
			JOptionPane.showMessageDialog(null, "Cancelled", "Warning", 2);
			return;
		}
		
		File instMods = new File("instMods");
		File coremods = new File("minecraft/coremods");
		File mods = new File("minecraft/mods");
		ArrayList<String> spareMods = new ArrayList<>();
		ArrayList<String> spareModsLinks = new ArrayList<>();
		ArrayList<String> downloadMods = new ArrayList<>();
		ArrayList<String> downloadModsLinks = new ArrayList<>();
		
		if(!instMods.exists()){
			instMods.mkdir();
		}
		if(!coremods.exists()){
			coremods.mkdirs();
		}
		if(!mods.exists()){
			mods.mkdirs();
		}
		String[][] data = loadConfigurationFile();
		if(data == null){
			System.out.println("Config file is empty!");
			return;
		}
		for(int i = 0; i < data.length; i++){
			StringBuilder sb = new StringBuilder(data[i][0]);
			String filename = data[i][0];
			String modname = data[i][1];
			String modlink = data[i][2];
			Boolean downloadAble = Boolean.parseBoolean(data[i][3]);
			String modtype = data[i][4];
			String dir = "";
			if(modtype.equals("instMod")){
				dir = "instMods/";
			}else if(modtype.equals("coreMod")){
				dir = "minecraft/coremods/";
			}else if(modtype.equals("mod")){
				dir = "minecraft/mods/";
			}else{
				System.out.println("Unknown mod type!");
			}
			
			if(doesExist(dir + filename)){
				System.out.println(modtype + " " + modname + " exists! (" + filename + ")");
			}else{
				System.out.println(modtype + " " + modname + " doesnt exist! (" + filename + ")");
				if(downloadAble){
					downloadMods.add(dir + filename);
					downloadModsLinks.add(modlink);
				}else{
					spareMods.add("(" + modtype.toUpperCase() + ") " + modname);
					spareModsLinks.add(modlink);
				}
				
			}
		}
		if(!downloadMods.isEmpty()){
			for(int i = 0; i < downloadMods.size(); i++){
				JOptionPane.showConfirmDialog(null, "");
				downloadFile(downloadModsLinks.get(i), downloadMods.get(i));
			}
		}
		if(!spareMods.isEmpty()){
			TestStuffGui.main(spareMods, spareModsLinks);
		}else{
			JOptionPane.showMessageDialog(null, "Update is complete.");
		}
		
	}
	
	
	public static String[][] loadConfigurationFile() throws IOException {
	    InputStream input = new FileInputStream(new File("config.yml"));
	    if(!new File("config.yml").exists()){
	    	input.close();
	    	return null;
	    }
	    Yaml yaml = new Yaml();
	    Object tmp = yaml.load(input);
	    if(tmp == null){
	    	return null;
	    }
	    String[] data = tmp.toString().split(",");
	    String[][] array = new String[data.length][5];
	    for(int i = 0; i < data.length; i++){
	    	
	    	if(i == 0){
	    	    StringBuilder sb = new StringBuilder(data[i]);
	    		data[i] = sb.deleteCharAt(0).toString();
	    	}else if(i == data.length -1){
	    		StringBuilder sb = new StringBuilder(data[i]);
	    		data[i] = sb.deleteCharAt(data[i].length() - 1).toString();
	    	}
	    	data[i] = data[i].trim();
	    	
	    	//Defining temporary arrays for easier declaration of variables
	    	String[] temp = data[i].split("=");
	    	String[] temp2 = temp[1].split(";");
	    	
	    	//Using StringBuilder to remove unwanted chars from read stuff.

	    	//Defining array variables
	    	array[i][0] = temp[0];
	    	array[i][1] = temp2[0];
	    	array[i][2] = temp2[1];
	    	array[i][3] = temp2[2];
	    	array[i][4] = temp2[3];
	    }
	    return array;

	}
	
	public static boolean doesExist(String filename){
		
		File file = new File(filename);
		
		if(file.exists()){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean downloadFile(String url, String targetname) throws IOException {
		 java.io.BufferedInputStream in = new java.io.BufferedInputStream(new
				 
				 URL(url).openStream());
				 java.io.FileOutputStream fos = new java.io.FileOutputStream(targetname);
				 java.io.BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
				 byte data[] = new byte[1024];
				 int i = 0;
				 int x = 0;
				 while((x=in.read(data,0,1024))>=0)
				 {
					 bout.write(data,0,x);
				 }

				 bout.close();
				 in.close();
				 System.out.println("Download : File '" + targetname + "' has sucessfully been downloaded.");
				 return true;
	}
}
