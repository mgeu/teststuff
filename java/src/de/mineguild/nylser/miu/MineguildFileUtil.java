package de.mineguild.nylser.miu;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URL;

public class MineguildFileUtil {
	
	public static void downloadFile(String url, String targetname) throws IOException { 
		java.io.BufferedInputStream in = new java.io.BufferedInputStream(new URL(url).openStream()); 
		java.io.FileOutputStream fos = new java.io.FileOutputStream(targetname);
		java.io.BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
		byte data[] = new byte[1024];
		int x = 0;
		int i = 0;
		while((x=in.read(data,0,1024))>=0)
		{
			bout.write(data,0,x);
			i++;
		}
		bout.close();
		in.close();
		System.out.println("Download: File '" + targetname + "(" + i + "kb)" + "' has sucessfully been downloaded.");
		return;
	}

}
