package de.mineguild.nylser.miu.xmlparse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MineguildHandler {
	
	public static String[][] handleConfigTag(Document document){
		String version = document.getElementsByTagName("version").item(0).getTextContent();
		System.out.println(version);
		
		NodeList modList = document.getElementsByTagName("mod");
		
		
		return handleMods(modList);		
	}
	
	
	public static String[][] handleMods(NodeList nodeList){
		String[][] array = new String[nodeList.getLength()][5];
		for(int i = 0; i < nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			
			if(node.getNodeType() == Element.ELEMENT_NODE){
				Element element = (Element) node;
				array[i][0] = handleTextTag("name", element);
				array[i][1] = handleTextTag("link", element);
				array[i][2] = handleTextTag("filename", element);
				array[i][3] = handleTextTag("downloadable", element);
				array[i][4] = handleTextTag("modtype", element);
			}
		}
		return array;
	}
	
	public static String handleTextTag(String tagName, Element element){
		StringBuffer returnValue = new StringBuffer();
		for(int i = 0; i < element.getElementsByTagName(tagName).getLength(); i++){
			NodeList nodeList = element.getElementsByTagName(tagName).item(i).getChildNodes();
			Node node = (Node) nodeList.item(0);
			returnValue = new StringBuffer(node.getNodeValue());
		}
//		System.out.println(returnValue.toString());
		return returnValue.toString();
	}

}
