package de.mineguild.nylser.miu;

import java.io.File;
import java.net.URL;

import de.mineguild.nylser.miu.xmlparse.MineguildDriver;
import de.mineguild.nylser.miu.MineguildFileUtil;
import org.apache.commons.io.FileUtils;

public class MineguildInstanceUpdate {
	
	public static void main(String args[]) throws Exception{
		
		Boolean needsUpdate;
		File cfgFile = new File("config.xml");
		File tmpFile = new File(".tmp");
		URL cfgUrl = new URL("http://update.mineguild.de/minecraft/config.xml");
		
		if(tmpFile.exists()){
			tmpFile.delete();
		}
		
		
		if(!cfgFile.exists()){
			needsUpdate = true;
			FileUtils.copyURLToFile(cfgUrl, cfgFile);
		}else{
			FileUtils.copyURLToFile(cfgUrl, tmpFile);
			if(FileUtils.contentEquals(cfgFile, tmpFile)){
				System.out.println("Configuration file up to date!");
				tmpFile.deleteOnExit();
			}else{
				System.out.println("Updating configuration file...");
				cfgFile.delete();
				FileUtils.moveFile(tmpFile, cfgFile);
				System.out.println("Configuration file sucessfully updated!");
				tmpFile.deleteOnExit();
				}
		}
		
		String[][] data = MineguildDriver.main();
		
		String modname;
		String modtype;
		String modlink;
		String modFileName;
		Boolean downloadable;
		
		for(int i = 0; i < data.length; i++){
			modname = data[i][0];
			modlink = data[i][1];
			modFileName = data[i][2];
			downloadable = Boolean.parseBoolean(data[i][3]);
			modtype = data[i][4];
			
			System.out.printf("%s %s %s %s \n", modname, modlink, modFileName, modtype);
			
			
			
		}
		
		
	}

}
